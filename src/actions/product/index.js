/*
 * @file: index.js
 * @description: It Contain Product Account Related Action Creators.
 * @author: 
 */

import * as TYPE from '../constants';
import ApiClient from '../../api-client';

export const get_products = (data) => ({ type: TYPE.GET_PRODUCT_LIST, data });
export const add_to_cart = data => ({ type: TYPE.ADD_TO_CART, data });
export const clear_cart = () => ({ type: TYPE.CLEAR_CART });
// Thunk Action Creators For Api
/****** action creator for login ********/
export const getProductListing = () => {    
    return dispatch => {
        ApiClient.get('http://localhost/apis/api/products').then(result => {
            console.log('result : ', result);
            if (result && result.data) {
               dispatch(get_products(result.data));
                }
        });
    };
};