/*
 * @file: constants.js
 * @description: It Contain action types Related Action.
 * @author: 
 */

/* export const ADD_TODO = 'ADD_TODO';
export const UPDATE_TODO = 'UPDATE_TODO';
export const DELETE_TODO = 'DELETE_TODO';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'; */
export const GET_PRODUCT_LIST = 'GET_PRODUCT_LIST';
export const ADD_TO_CART = 'ADD_TO_CART';
export const CLEAR_CART = 'CLEAR_CART';
export const ORDER_NOW = 'ORDER_NOW';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';